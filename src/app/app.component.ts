import { Component } from '@angular/core';
import { BaseService } from './services/base.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Outreach-Reporting-System';
  dataLoading:boolean;

  constructor(private baseSvc: BaseService) {
      baseSvc.loading.subscribe(flag => this.showLoading(flag));
  }

  // showErrorMessage(msg: string):void{
  //   if(msg)
  //   alert(msg);
  // }
  showLoading(flag: boolean):void{
    this.dataLoading=flag;
  }
  ngOnInit(){
  }
}
