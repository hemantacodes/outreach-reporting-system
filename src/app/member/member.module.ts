import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule , JsonpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { memberRoutes } from './member.routes';
import { CKEditorModule } from 'ng2-ckeditor';
import { FormsModule } from '@angular/forms';

import { NavbarComponent } from './navbar/navbar.component';
import { AssetviewComponent} from './assetview/assetview.component';
import { HomeComponent } from './home/home.component';
import { AssetfeedbacksComponent } from './assetfeedbacks/assetfeedbacks.component';
import { AssetlogsComponent } from './assetlogs/assetlogs.component';

import { AgGridModule } from 'ag-grid-angular/main';
import { CampaignGraphComponent } from './campaign-graph/campaign-graph.component';
import { ChartsModule } from 'ng2-charts';
import { PrintAssetTemplateComponent } from './print-asset-template/print-asset-template.component';





@NgModule({
  declarations: [
   NavbarComponent,
   HomeComponent,
   AssetviewComponent,
   AssetfeedbacksComponent,
   AssetlogsComponent,
   CampaignGraphComponent,
   PrintAssetTemplateComponent
  
   ],
  imports: [
    FormsModule,
    memberRoutes,
    CommonModule,
    CKEditorModule,
    AgGridModule.withComponents([AssetlogsComponent]),
    ChartsModule

  ]

})
export class MemberModule { }