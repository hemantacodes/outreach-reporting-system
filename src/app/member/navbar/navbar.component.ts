import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { BaseService } from '../../services/base.service';
import { CreateCampaignModalComponent } from '../../create-campaign-modal/create-campaign-modal.component';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import {StateData} from '../../providers/StateData';
import { UtilService } from '../../services/util.service';

declare var $;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public lastLogin: string = "";

  constructor( public utilService: UtilService, public stateData: StateData, public baseService : BaseService, public ss : SharedService,public router : Router,public modal : Modal) {
    if( stateData.loggedUser == null)
      this.stateData = baseService.getInitialStateData();
   }

  //flag2=this.ss.flag_forgot_password;
  ngOnInit() {
    //this.lastLogin = this.stateData.lastLogin;
    // this.router.navigate(['./member/home'])
     $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );
  }
  Change_password()
{
    // this.ss.flag_change_password=true;
    // this.ss.flag_forgot_password=false;
    // this.ss.flag_terminate=false;
    this.utilService.setConfirmModalArg("flag_change_password");
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
}
Terminate_account(){
  // this.ss.flag_terminate=true;
  // this.ss.flag_change_password=false;
  // this.ss.flag_forgot_password=false;
  this.utilService.setConfirmModalArg("flag_terminate");
   return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
}
logOut(){
  // if(this.ss.currentAssetObj){
  //   this.ss.currentAssetObj = undefined;
  // }
  localStorage.clear();
  this.router.navigate(['./app-login'])
}


}
