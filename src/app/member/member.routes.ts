import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component'
import { AssetviewComponent} from './assetview/assetview.component'
import { HomeComponent } from './home/home.component'
import { AssetfeedbacksComponent } from './assetfeedbacks/assetfeedbacks.component';
import { AssetlogsComponent } from './assetlogs/assetlogs.component';
import { CampaignGraphComponent } from './campaign-graph/campaign-graph.component';
import { PrintAssetTemplateComponent } from './print-asset-template/print-asset-template.component';


export const memberRouter : Routes = [
    
    {path : '' , component : NavbarComponent,
        children:[{path:'',redirectTo:'home' , pathMatch:'full'},
                  {path : 'home' , component : HomeComponent},
                  {path : 'home/:currentSelectedCampaignName' , component : HomeComponent},
                  {path : 'asset-view/:campaignId/:assetId' , component : AssetviewComponent},
                  {path : 'asset-view/:campaignId' , component : AssetviewComponent},
                  {path : 'asset-feedbacks' , component : AssetfeedbacksComponent},
                  {path : 'asset-feedbacks/asset/:assetId' , component : AssetfeedbacksComponent},
                  {path : 'asset-logs' , component : AssetlogsComponent},
                  {path : 'asset-logs/asset/:assetId' , component : AssetlogsComponent},
                  {path : 'campaign-graph/campaign/:campaignId', component : CampaignGraphComponent},
                  {path : 'print-asset-template', component : PrintAssetTemplateComponent}
                ]
            },
    
] 

export const memberRoutes : ModuleWithProviders = RouterModule.forChild(memberRouter); 