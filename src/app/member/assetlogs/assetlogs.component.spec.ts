import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetlogsComponent } from './assetlogs.component';

describe('AssetlogsComponent', () => {
  let component: AssetlogsComponent;
  let fixture: ComponentFixture<AssetlogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetlogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetlogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
