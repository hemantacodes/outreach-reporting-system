import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {GridOptions} from "ag-grid";
import { MockService } from '../../services/mock.service';
import { SharedService } from '../../services/shared.service';
import {StateData} from '../../providers/StateData';
import { CampaignService} from '../../services/campaign.service';

@Component({
  selector: 'app-assetlogs',
  templateUrl: './assetlogs.component.html',
  styleUrls: ['./assetlogs.component.css']
})
export class AssetlogsComponent implements OnInit {

  public gridOptions : GridOptions;
  public rowData : any = [];

  public currentSelectedAssetId : string;

  constructor(public stateData : StateData, public campaignSvc : CampaignService, public activeRoute : ActivatedRoute ,public router : Router , public mockSvc : MockService , public sharedSvc : SharedService ) { 
    this.gridOptions = {
      rowSelection: 'single',
      columnDefs : [
        {
                headerName: "IP Address",
                field: "ipAddress",
                width : 500,
                
               
        },
        {
                headerName: "Date",
                field: "createdDate",
                width : 500,
               
        }
      ],
      onGridReady : function(){
        this.gridOptions.api.setRowData(this.rowData);
      }
    }
    
  }

  ngOnInit() {
    //this.rowData = this.mockSvc.logArray;
    //this.gridOptions.api.setRowData(this.rowData);
  
   /////////////////////////
   this.loadAllScanLogs();
  }

  loadAllScanLogs(){
        this.activeRoute.params.subscribe(params=>{
        this.currentSelectedAssetId =params['assetId'];

        if(this.currentSelectedAssetId){
          this.campaignSvc.getScanLogByAssetId(this.currentSelectedAssetId).then(res=>{
            
            console.log(res.data);
            if(res.data){
              this.rowData = res.data.scanLog;
              console.log(this.rowData.length)
            }
            else{
              this.rowData = [];
            }
          });
        }

    })
  }

  goToPrevious(){
    //this.router.navigate(['./member/asset-view/'+this.currentSelectedCampaignId+"/"+this.assetId ]);
    //this.router.navigate([this.sharedSvc.previousPage]);
    this.router.navigate([this.stateData.previousPage]);
  }
}
