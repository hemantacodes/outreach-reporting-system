import { Component, EventEmitter,OnInit,Output } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';

import{SharedService} from '../../services/shared.service';
import{CampaignModel} from '../../model/campaign.model';
import {Constants} from '../../model/constants.model';
import {StateData} from '../../providers/StateData';
import {Asset} from '../../model/Asset.model';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-print-asset-template',
  templateUrl: './print-asset-template.component.html',
  styleUrls: ['./print-asset-template.component.css']
})
export class PrintAssetTemplateComponent implements OnInit {
  public qrCodeImageBaseURL: string = environment.BASE_IMAGE_URL;
  public asset : Asset;
  constructor(public stateData: StateData, public ss : SharedService) { }

  ngOnInit()
   {this.asset = {
      id:Constants.NULL_GUID,
      name:"",
      metaData:"",
      qrCode:"",
      qrCodeImageURL:"",
      feedbackCount : 0,
      viewCount : 0
    }


    this.loadAsset();
  }

  loadAsset(){
    if(this.stateData.selectedAsset){
      this.asset = this.stateData.selectedAsset;
    }
  }

  onClose() {
    //this.dialog.close();
    // $('body').removeClass('modal-open');
    // // this.printAllQR =  false;
    // this.ss.printAllQR = false;
  }
  print_Asset(){
//  console.log(this.printable)

  this.onClose();
  }
}
