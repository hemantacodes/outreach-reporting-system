import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintAssetTemplateComponent } from './print-asset-template.component';

describe('PrintAssetTemplateComponent', () => {
  let component: PrintAssetTemplateComponent;
  let fixture: ComponentFixture<PrintAssetTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintAssetTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintAssetTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
