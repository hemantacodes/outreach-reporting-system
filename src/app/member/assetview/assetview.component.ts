import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { MockService } from '../../services/mock.service';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import {PrintAssetModalComponent} from '../print-asset-modal/print-asset-modal.component'
import{CampaignService} from '../../services/campaign.service';
import {Asset} from '../../model/Asset.model';
import {CampaignModel} from '../../model/campaign.model';
import {Constants} from '../../model/constants.model';
import {StateData} from '../../providers/StateData';
import { ToastrService } from 'toastr-ng2';

@Component({
  selector: 'app-assetview',
  templateUrl: './assetview.component.html',
  styleUrls: ['./assetview.component.css']
})
export class AssetviewComponent implements OnInit {
ckeditorContent:any;
//public asset : Asset;

public editForm : boolean = false;
public toBeEdited : boolean = false;
//public hasSubscrition : boolean = false;
public hasQRCode : boolean = false;
asset_des;
//////////////\\\\\\\\/////////
public assetViewRoute : any;
public assetId: string = Constants.NULL_GUID;
public campaignId: string="";
//public campaignName: string ="No campaigns found!";
//public currentCampaignObj : CampaignModel;

constructor(public stateData: StateData, public activeRoute : ActivatedRoute, public campaignSvc : CampaignService, public sharedSvc: SharedService, public modal : Modal,public mockSvc : MockService ,  public router : Router, public toaster : ToastrService) { }

  ngOnInit() {
    
    //__//\\\//__//
    
    //this.currentCampaignObj = new CampaignModel() ;
    //this.asset = new Asset();
    this.loadCampaignIdAndAssetIdByRouteParam();
  }


  //\\//

  loadCampaignIdAndAssetIdByRouteParam(){

        this.assetViewRoute = this.activeRoute.params.subscribe( params => {
        this.campaignId = params['campaignId'];
        
        this.assetId = params['assetId'];
        

       //to set campaignName
      this.campaignSvc.getCampaignById(this.campaignId).then(res=>{

        this.stateData.selectedCampaign = res.data.campaign;  
        console.log(this.stateData.selectedCampaign);
        //this.campaignName = this.currentCampaignObj.campaignName;
        
        //console.log(this.campaignName);
        if(this.stateData.selectedCampaign.isValidSubscription){
              //this.hasSubscrition= true;
        }
      })
      /** if assetId is true load this page as view asset / edit asset */
      this.loadAsset();
    })
  }

  loadAsset(){
      if(this.assetId){
        //this.loadAssetInfo(this.assetId);
        this.campaignSvc.getAssetById(this.campaignId,this.assetId).then(res=>{
          //this.asset = res.data;
          console.log("res.data.asset");
          console.log(res.data.asset);
          this.stateData.selectedAsset = res.data.asset;
          console.log("this.asset");
          console.log(this.stateData.selectedAsset);
          if(this.stateData.selectedAsset.qrCode!= null){
            this.hasQRCode = true;
          }

        })
        this.toBeEdited = true;
      }
      /**else load this page for creating an Asset*/
      else{
        this.stateData.selectedAsset={
              id:Constants.NULL_GUID,
              name:"",
              metaData:"",
              qrCode:"",
              qrCodeImageURL:"",
              feedbackCount : 0,
              viewCount : 0
            }
            this.toBeEdited = false;
          }
  }


  editAsset(){
    this.editForm = true;
    this.toBeEdited = false;
  }


      saveAssetForCurrCampaign(){
        if(!this.assetId){
            console.log(this.stateData.selectedAsset);
            console.log(this.campaignId );
            this.campaignSvc.createAsset(this.campaignId,this.stateData.selectedAsset).then(res=>{
              console.log( this.campaignId + this.stateData.selectedAsset + "asset created");
            console.log( res);
            if(res.status){
              this.toaster.success('Asset saved successfully');
              this.assetId = res.data;
              this.loadAsset();

            }
            else{
              this.toaster.error('Asset not saved');
            }

          })
        }
        else{
            this.campaignSvc.updateAsset(this.campaignId,this.stateData.selectedAsset).then(res=>{
              console.log( this.campaignId + this.stateData.selectedAsset + "asset modified");
              console.log( res);
              if(res.status){
                this.toaster.success('Asset saved successfully');
                this.loadAsset();
              }
              else{
                this.toaster.error('Changes not saved');
              }
            })

            this.editForm = false;
            this.toBeEdited = true;
        }

      }


   generateQRCode(){
//     this.sharedSvc.currentAssetObj.hasQRCode = true;
//     this.hasQRCode = true;
        if(this.assetId && this.campaignId){
            this.campaignSvc.getQRCode(this.campaignId, this.assetId).then(res=>{
              console.log(res.data);
              this.loadAsset();
              if(res.status){
                this.toaster.success('QrCode created successfully');
              }
              else{
                this.toaster.error('Unsuccessful Qr-Code creation');
              }
            })
        }

   }

  goToPrevious(){

    /*if(this.currentCampaignObj){
      this.router.navigate(['./member/home/'+this.currentCampaignObj.campaignName]); 
    }
    else{
      this.router.navigate(['./member/home']);
    }*/
    this.router.navigate(['./member/home']);
  }

  goToFeedback(){
    //this.stateData.previousPage = './member/asset-view/'+this.currentCampaignObj.id+'/'+this.assetId;
    //this.router.navigate(['./member/asset-view/'+this.currentSelectedCampaignId+"/"+this.assetId ]);
    this.stateData.previousPage = './member/asset-view/'+this.campaignId+"/"+this.assetId;
    this.router.navigate(['./member/asset-feedbacks/asset/'+this.assetId]);
    //this.stateData.previousPage ='asset-view/'+this.campaignId+'/'+this.assetId;
  }
  goToViewLogs(){
    this.stateData.previousPage = './member/asset-view/'+this.campaignId+"/"+this.assetId;
    this.router.navigate(['./member/asset-logs/asset/'+this.assetId]);
  }
  
print_asset(assetObj){
  //this.sharedSvc.asset_to_be_printed=assetObj;
  //this.sharedSvc.flag_print_asset=true;
  return this.modal.open(PrintAssetModalComponent,  overlayConfigFactory({}, BSModalContext));
}

  cancel_view(){
    
    /*if(this.currentCampaignObj){
      this.router.navigate(['./member/home/'+this.currentCampaignObj.campaignName]); 
    }
    else{
      this.router.navigate(['./member/home']);
    }*/

    this.router.navigate(['./member/home']);
  }
}