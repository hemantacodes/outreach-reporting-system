import { Component, OnInit , ViewContainerRef , OnDestroy } from '@angular/core';
//import { Subscription } from 'rxjs/Rx';
import{CampaignService} from '../../services/campaign.service';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { Overlay, overlayConfigFactory } from 'angular2-modal';

import{CampaignModel} from '../../model/campaign.model';
import {Asset} from '../../model/Asset.model';
import {Constants} from '../../model/constants.model';
import {Subscription} from '../../model/subscription.model';

import{AssetService} from '../../services/asset.service';
import{MockService} from '../../services/mock.service';
import { SharedService } from '../../services/shared.service';
import { UtilService } from '../../services/util.service';
import {StateData} from '../../providers/StateData';
import { CreateCampaignModalComponent } from '../../create-campaign-modal/create-campaign-modal.component';
import { AccountModalComponent } from '../../account-modal/account-modal.component';
//import {PrintAssetComponent} from '../../print-asset/print-asset.component';

import { environment } from '../../../environments/environment';

import $ from 'jquery';
declare var _ : any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
 })

export class HomeComponent  implements OnInit,OnDestroy {

  //public assetListForSelCampaign : any = [];
  public hasSubscription : boolean = false;
  public subscriptionExpiryDate : any;
  /////////////\\\\\\\\////////////////////////////
  

  //public campaignList:any = [];
  //public assetList: Array<Asset>;
  //public currentSelectedCampaign : CampaignModel;

  //public assetId: string ="";
  public qrCodeImageBaseURL: string = environment.BASE_IMAGE_URL;

  constructor(public stateData: StateData, public activeRoute : ActivatedRoute,public router : Router ,public ss : SharedService ,private mock_service:MockService,public utilService: UtilService,private asset_service:AssetService,private campaignService:CampaignService , public modal : Modal , overlay: Overlay,vcRef: ViewContainerRef ) 
  {

  }


  ngOnInit() {
    //this.currentSelectedCampaign = new CampaignModel();
    
    //this.stateData.selectedAsset
    if(!this.stateData.selectedCampaign){
      var subscrptn = new Subscription();
      this.stateData.selectedCampaign = {
        id:Constants.NULL_GUID,
        campaignName: "",
        //Account: any;
        assetList: [],
        subscription: subscrptn,
        isValidSubscription : false
      }
    }
    if(!this.stateData.campaignList){
      this.stateData.campaignList = [];
    }
    this.check(this.stateData.selectedCampaign.campaignName);
  }

  // getAllCampaign(){
  //   if(this.stateData.campaignList.length<1){
  //     this.campaignService.getCampaignList().then(res=>{
  //       //this.campaignList = res.data.campaigns;
  //       this.stateData.campaignList =res.data.campaigns;
  //       console.log(this.stateData.campaignList);
  //       //this.loadCampaign();
  //     })
  //   }
  //   else{
  //     //this.loadCampaign();
  //   }
  // }


  check(aCampaignName){

    if(!this.stateData.selectedCampaign || this.stateData.selectedCampaign.campaignName.length==0 || !this.stateData.campaignList || this.stateData.campaignList.length ==0){
      this.campaignService.getCampaignList().then(res=>{
        this.stateData.campaignList =res.data.campaigns;
          if(this.stateData.campaignList.length>0){
            this.loader(this.stateData.campaignList[0].campaignName)
          }
      })
    }
    else{
      this.loader(aCampaignName);
    }

  }

  loader(aCampaignName){
    for(var i =0; i<this.stateData.campaignList.length;i++){
      if(aCampaignName == this.stateData.campaignList[i].campaignName){
            this.campaignService.getCampaignById(this.stateData.campaignList[i].id).then(res=>{
              console.log(res.data);
              this.stateData.selectedCampaign =res.data.campaign;
              //this.assetListForSelCampaign = this.stateData.selectedCampaign.assetList;
              this.hasSubscription = this.stateData.selectedCampaign.isValidSubscription;
              if(this.stateData.selectedCampaign.subscription!=null){
                this.subscriptionExpiryDate = res.data.campaign.subscription.expiryDate;
              }
            })
      }
    }
  } 

  ngOnDestroy(){
    //this.assetTobeDelSub.unsubscribe();
  }

  collapseList(val){
      $(val).collapse('toggle');     
  }


  // populateAssets(){
  //   this.assetListForSelCampaign = this.campaignList[0];
  // }

  openCreateCampaignModal(){
    //this.ss.flag_create_campaign=true;
    this.utilService.setConfirmModalArg("flag_create_campaign");
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
  }

  // print_asset(assetObj)
  // {
  //   if(assetObj.hasQRCode){
  //     this.ss.flag_print_asset=true;
      
  //   }
  //   else{
  //     this.ss.flag_print_asset=false;
  //   }
  //   this.ss.asset_to_be_printed=assetObj;
  //   return this.modal.open(PrintAssetComponent,  overlayConfigFactory({}, BSModalContext));
  // }  
  
  remove_asset(assetObj){
    //this.ss.flag_delete_asset=true;
    this.utilService.setConfirmModalArg("flag_delete_asset");
    //this.stateData.selectedCampaign = this.currentSelectedCampaign;
    this.stateData.selectedAsset = assetObj;
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
  }
  
  createAssetForSelectedCampaign(){
    var assetId="";
    //this.router.navigate(['./member/asset-view/'+this.currentSelectedCampaign.id+"/"+this.assetId ]);
    this.router.navigate(['./member/asset-view/'+this.stateData.selectedCampaign.id+"/"+assetId ]);
  }
  
  goToView(aId){
    //this.campaignService.getAssetById(this.currentSelectedCampaign.id, aId).then(res=>{
      this.campaignService.getAssetById(this.stateData.selectedCampaign.id, aId).then(res=>{
      this.stateData.selectedAsset = res.data.asset;
      //this.router.navigate(['./member/asset-view/'+this.currentSelectedCampaign.id+"/"+aId ]);
      this.router.navigate(['./member/asset-view/'+this.stateData.selectedCampaign.id+"/"+aId ]);
    });
    
    
  }

  addSubscription(){
    //this.campaignService.getSubscription(this.currentSelectedCampaign.id).then(res=>{
      this.campaignService.getSubscription(this.stateData.selectedCampaign.id).then(res=>{
      console.log(res.data);
      this.check(this.stateData.selectedCampaign.campaignName);
    })

  }

  deleteCampaign(){
    //this.ss.flag_delete_single_campaign=true;
    this.utilService.setConfirmModalArg("flag_delete_single_campaign");
    //this.stateData.selectedCampaign = this.currentSelectedCampaign;
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
  }

  printAllQR(){
    //this.ss.printAllQR = true;
    //this.ss.currAssetsList = this.assetListForSelCampaign;
    //return this.modal.open(PrintAssetComponent,  overlayConfigFactory({}, BSModalContext));
    this.campaignService.printAllAsset(this.stateData.selectedCampaign.id).then(res=>{
      window.location.href = res.data;
    })
  }

  goToFeedback(assetId){
      // this.campaignService.getAssetbyIdWithoutCampaignId(assetId).then(res=>{
        
      // })
      this.router.navigate(['./member/asset-feedbacks/asset/'+assetId]);
      this.stateData.previousPage = "./member/home";
    }
  goToViewLogs(assetId){
      //this.assetId= assetId;
      this.stateData.previousPage = './member/home';
      this.router.navigate(['./member/asset-logs/asset/'+assetId]);
  }
  openCampaignGraph(){  
      //this.ss.currentCampName = this.currentSelectedCampaign.campaignName;
      //this.ss.currentCampName = this.stateData.selectedCampaign.campaignName;
      this.router.navigate(['./member/campaign-graph/campaign/'+this.stateData.selectedCampaign.id]);
      this.stateData.previousPage = './member/home';
  }
  
}

