import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { MockService } from '../../services/mock.service';
import { ChartsModule } from 'ng2-charts';
import {StateData} from '../../providers/StateData';
import { CampaignService} from '../../services/campaign.service';


@Component({
  selector: 'app-campaign-graph',
  templateUrl: './campaign-graph.component.html',
  styleUrls: ['./campaign-graph.component.css']
})
export class CampaignGraphComponent implements OnInit {

  public campaignName: string;
  

  //public barChartLabels:string[] = ['2010', '2011', '2012', '2013', '2014', '2015', '2016'];
  public barChartLabels:string[] = [];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = false;

  /*public barChartData:any[] = [
    //{data: [65, 59, 80, 81, 56, 55, 40], label: this.campaignName}
    {data: [65, 59, 80, 81, 56, 55, 40], label: this.campaignName}
  ];*/

  public barChartData:any[] = [
    //{data: [65, 59, 80, 81, 56, 55, 40], label: this.campaignName}
    {data: []}
  ];

  ////////////////////////////////////
  public currentSelectedCampaignId: string;

  constructor(public stateData : StateData, public campaignSvc : CampaignService, public activeRoute : ActivatedRoute ,public router : Router , public myChartsModule: ChartsModule) { }

  ngOnInit() {

    this.campaignName = "";
    //this.barChartLabels = this.sharedSvc.currentCampObj
    console.log(this.campaignName);

      // this.barChartData = [{data: [65, 59, 80, 81, 56, 55, 40], label: this.campaignName}];
      //this.barChartLabels = ['A Sunday Afternoon on the Island of La Grande Jatte','Girl with a Pearl Earring','Portrait de L artiste Sans Barbe','Cafe Terrace at Night','Asset 5','Asset 6'];
      // this.barChartData = [{data: []}];
      // this.barChartLabels = [];
     
      this.loadCampaignGraph();
  }

  loadCampaignGraph(){
    console.log(this.stateData.selectedCampaign);
    this.activeRoute.params.subscribe( params => {
      this.currentSelectedCampaignId = params['campaignId'];
    })
      // this.campaignSvc.getAssetList(this.stateData.selectedCampaign.id).then(res=>{
      //   var assetArr : number[];
      //   assetArr = res.data.assets;
      //   assetArr.forEach(element => {
      //     this.barChartData[0].data.push()
      //     //this.barChartLabels.push(assetArr.element.viewCount);
      //   });
      //   console.log(this.barChartData);
      // })
      this.campaignSvc.getCampaignById(this.currentSelectedCampaignId).then(res=>{
        if(!this.stateData.selectedCampaign){
          this.stateData.selectedCampaign = res.data.campaign;
        }
        this.campaignName = this.stateData.selectedCampaign.campaignName;
        var assetArr : any[];
        var viewCount : number[] = [];
        var assetName : string ="";

        assetArr = res.data.campaign.assetList;
        for(var i=0; i< assetArr.length; i++){
          viewCount.push(assetArr[i].viewCount);
          this.barChartLabels.push(assetArr[i].name);
        }
        this.barChartData = [{data:viewCount}];
        console.log(this.barChartData);
      })
  }
  
  goToPrevious(){
    this.router.navigate(['./member/home']);
  }

    public chartColors: Array<any> = [
    { // first color
      backgroundColor: 'rgba(6, 71, 112,0.2)',
      borderColor: 'rgba(6, 71, 112,0.2)',
      pointBackgroundColor: 'rgba(6, 71, 112,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(6, 71, 112,0.2)'
    },
    { // second color
      backgroundColor: 'rgba(6, 71, 112,0.2)',
      borderColor: 'rgba(6, 71, 112,0.2)',
      pointBackgroundColor: 'rgba(6, 71, 112,0.2)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(6, 71, 112,0.2)'
    }];

    public barChartOptions:any = {
      scaleShowVerticalLines: false,
      responsive: true,


      scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true,
                stepSize: 1
                //maxTicksLimit: 1,
                // Create scientific notation labels
                // callback: function(value, index, values) {
                //     return value + ' €';
                // }
                //min: 0,
                //max: 100,
            }
        }],
        xAxes: [{
            //categoryPercentage: 1.0,
            //barPercentage: 0.6
        }]
    }
  };
}
