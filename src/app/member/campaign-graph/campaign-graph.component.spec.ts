import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignGraphComponent } from './campaign-graph.component';

describe('CampaignGraphComponent', () => {
  let component: CampaignGraphComponent;
  let fixture: ComponentFixture<CampaignGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
