import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintAssetModalComponent } from './print-asset-modal.component';

describe('PrintAssetComponent', () => {
  let component: PrintAssetModalComponent;
  let fixture: ComponentFixture<PrintAssetModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintAssetModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintAssetModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
