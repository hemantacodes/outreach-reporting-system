import { Component, EventEmitter,OnInit,Output } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import{MockService} from '../../services/mock.service';
import{SharedService} from '../../services/shared.service';
import{CampaignModel} from '../../model/campaign.model';
import {Constants} from '../../model/constants.model';
import {StateData} from '../../providers/StateData';
import {Asset} from '../../model/Asset.model';
import { environment } from 'environments/environment';

declare var $;

export class CustomModalContext extends BSModalContext {
  
} 

@Component({
  selector: 'app-print-asset',
  templateUrl: './print-asset-modal.component.html',
  styleUrls: ['./print-asset-modal.component.css']
})
export class PrintAssetModalComponent implements OnInit , CloseGuard, ModalComponent<CustomModalContext>{
// context: CustomModalContext;
// printable:any;
// flag_print_asset: boolean = false;
// printAllQR : boolean = false;
// filteredAssetList : any = [];
  public qrCodeImageBaseURL: string = environment.BASE_IMAGE_URL;
  public asset : Asset;
  constructor(public router:Router, public stateData: StateData, public ss : SharedService , private mock_service:MockService,public dialog: DialogRef<CustomModalContext>) 
  { }

  
  ngOnInit() {
    // this.printable=this.ss.asset_to_be_printed;
    // this.flag_print_asset=this.ss.flag_print_asset;
    // if(this.ss.printAllQR){
    //   this.printAllQR = true;
    //   for(let i = 0 ; i < this.ss.currAssetsList.assets.length ; i++){
    //     if(this.ss.currAssetsList.assets[i].hasQRCode){
    //       this.filteredAssetList.push(this.ss.currAssetsList.assets[i]);
    //     }
    //   }
    // }
    this.asset = {
      id:Constants.NULL_GUID,
      name:"",
      metaData:"",
      qrCode:"",
      qrCodeImageURL:"",
      feedbackCount : 0,
      viewCount : 0
    }


    this.loadAsset();
  }

  loadAsset(){
    if(this.stateData.selectedAsset){
      this.asset = this.stateData.selectedAsset;
    }
  }

  onClose() {
    this.dialog.close();
    // $('body').removeClass('modal-open');
    // // this.printAllQR =  false;
    // this.ss.printAllQR = false;
  }
  print_Asset(){
//  console.log(this.printable)
    //this.router.navigate(['./member/print-asset-template']);
    //var templateContent = $('#modal_body').html();
    
    var wObj = window.open();
    wObj.document.write($('#modal_body').html());
    wObj.print();
  
  }
}
