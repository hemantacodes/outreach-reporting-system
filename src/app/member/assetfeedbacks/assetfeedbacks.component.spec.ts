import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetfeedbacksComponent } from './assetfeedbacks.component';

describe('AssetfeedbacksComponent', () => {
  let component: AssetfeedbacksComponent;
  let fixture: ComponentFixture<AssetfeedbacksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetfeedbacksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetfeedbacksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
