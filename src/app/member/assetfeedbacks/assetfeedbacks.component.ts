import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import {StateData} from '../../providers/StateData';
import { CampaignService} from '../../services/campaign.service';
import { CreateCampaignModalComponent } from '../../create-campaign-modal/create-campaign-modal.component';
import { ToastrService } from 'toastr-ng2';
import { UtilService } from '../../services/util.service';
import {Asset} from '../../model/Asset.model';
import {Feedback} from '../../model/feedback.model';

@Component({
  selector: 'app-assetfeedbacks',
  templateUrl: './assetfeedbacks.component.html',
  styleUrls: ['./assetfeedbacks.component.css']
})
export class AssetfeedbacksComponent implements OnInit {

  //feedbackArr : string[];
  currentSelectedAssetId : string ;

  constructor(public utilService: UtilService, public stateData : StateData, public toastrSvc : ToastrService, public campaignSvc : CampaignService, public activeRoute: ActivatedRoute, public router : Router, public ss : SharedService , public modal : Modal, overlay: Overlay ) {

    //this.feedbackArr = [];
    this.stateData.feedbackList = [];
   }

  ngOnInit() {

    this.loadAllFeedback();

  }

  loadAllFeedback(){
    this.activeRoute.params.subscribe(params=>{
        this.currentSelectedAssetId =params['assetId'];

        if(this.currentSelectedAssetId && (!this.stateData.selectedAsset || this.stateData.selectedAsset.name.length==0)){
          this.campaignSvc.getAssetbyIdWithoutCampaignId(this.currentSelectedAssetId).then(res=>{
            this.stateData.selectedAsset = res.data.asset;
            console.log(res.data.asset);
          })
        }

        if(this.currentSelectedAssetId){
          this.campaignSvc.getAllAssetFeedback(this.currentSelectedAssetId).then(res=>{
            console.log(res.data.feedback);
            
            if(res.data.feedback){
              this.stateData.feedbackList = res.data.feedback;
              console.log(this.stateData.feedbackList.length)
            }
            else{
              this.stateData.feedbackList = [];
            }
          });
        }

    })
    
}
  goToPrevious(){
    // this.router.navigate(['./member/asset-view']);
    console.log(this.stateData.previousPage);
    this.router.navigate([this.stateData.previousPage]);
    
  }
  delSingleFeedback(aFeedback : Feedback){
    //var feedbackId = aFeedback.id;
    // this.campaignSvc.deleteSingleFeedbackById(this.currentSelectedAssetId, aFeedback.id).then(res=>{
    //   console.log(res);
    //   if(res.status){
    //     this.toastrSvc.success("The feedback is deleted");
    //   }
    //   else{
    //     this.toastrSvc.error("Error on deleting the feedback");
    //   }
    // })
    this.stateData.selectedFeedback = aFeedback;

    //this.ss.flag_single_feedback_delete = true;
    this.utilService.setConfirmModalArg("flag_single_feedback_delete");
    //this.stateData.selectedCampaign = this.currentSelectedCampaign;
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
  }
  deleteAllFeedback(){
    //this.ss.flag_all_feedback_delete = true;
    this.utilService.setConfirmModalArg("flag_all_feedback_delete");
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
  }
}
