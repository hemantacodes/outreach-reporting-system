import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { guestRoutes } from './guest.route';
import { NavbarComponent } from './navbar/navbar.component';
import { AssetDisplayComponent } from './asset-display/asset-display.component';
import { FormsModule } from '@angular/forms';
import { CKEditorModule } from 'ng2-ckeditor';
import { ChartsModule } from 'ng2-charts';



@NgModule({
  imports: [
    CommonModule,
    guestRoutes,
    CommonModule,
    CKEditorModule,
    guestRoutes,
    ChartsModule,
    FormsModule 
  ],
  declarations: [
        AssetDisplayComponent,
        NavbarComponent
  ]
})
export class GuestModule { }
