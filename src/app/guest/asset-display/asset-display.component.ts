import { Component, OnInit } from '@angular/core';

import{CampaignService} from '../../services/campaign.service';
import {Asset} from '../../model/Asset.model';
import {CampaignModel} from '../../model/Campaign.model';
import {Feedback} from '../../model/Feedback.model';

import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'toastr-ng2';

@Component({
  selector: 'app-asset-display',
  templateUrl: './asset-display.component.html',
  styleUrls: ['./asset-display.component.css']
})
export class AssetDisplayComponent implements OnInit {

  public asset : Asset;
  //public feedback : Feedback;
  public qrCodeId: string;
  public assetId : "";
  public isFeedbackLive: boolean = true;
  public noFeedbackSending :boolean = false;
  public feedbackStr : string = "";
  
  constructor(public activeRoute : ActivatedRoute, public campaignSvc: CampaignService,  public toaster : ToastrService) { }

  ngOnInit() {
    this.asset = new Asset();
    //this.feedback = new Feedback();
    this.feedbackStr = "";
    this.loadAssetDetails();
  }

  loadAssetDetails(){
    this.activeRoute.params.subscribe( params => {
      this.assetId = params['assetId'];
      //this.asset = this.campaignSvc.getAssetById;
      //this.asset = this.campaignSvc.getAssetById;
      this.campaignSvc.getAssetbyIdWithoutCampaignId(this.assetId).then(res=>{
        //console.log(res.data)
        if(res.status){
          if(res.data){
            this.isFeedbackLive = true;
            this.asset = res.data.asset;
          }
          else{
            this.isFeedbackLive =false;
          }
        }
      })
    })
  }

  sendFeedback(){
    if(this.isFeedbackLive){
       //console.log(this.feedbackStr.trim());
      if(this.feedbackStr.trim().length>0){
        this.campaignSvc.sendFeedback(this.assetId,JSON.stringify(this.feedbackStr)).then(res=>{
          if(res.status){
            this.toaster.success("Comment sent successfully.")
            this.feedbackStr="";
            this.noFeedbackSending = true;
          }
          else{
            this.toaster.error("Error sending comment.")
          }
        });
      }
    }
  }
}
