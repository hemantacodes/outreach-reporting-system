import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import {AssetDisplayComponent} from './asset-display/asset-display.component';
import {NavbarComponent} from './navbar/navbar.component';

export const guestRouter : Routes = [
    {path : '' , component : NavbarComponent,
        children:[
            //{path:'',redirectTo:'asset-display' , pathMatch:'full'},
            {path : 'asset-display' , component : AssetDisplayComponent},
            {path : 'asset-display/:assetId' , component : AssetDisplayComponent} 
        ]
    }
]

export const guestRoutes : ModuleWithProviders = RouterModule.forChild(guestRouter); 