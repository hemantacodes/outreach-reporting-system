import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountModalComponent } from '../account-modal/account-modal.component';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { SharedService } from '../services/shared.service';
    
@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  istruelog="true";
  istruereg="false";
  isAccountRecover = "false";
  


  constructor(public ss : SharedService,public router : Router,public modal : Modal) { }

  doLogin(){
     this.router.navigate(['./member/home']);
  }
  ngOnInit() {
    //this.router.navigate(['./app-login'])  
  }
  log(){
  //   this.isAccountRecover = "false";
    this.istruereg="false";
    this.istruelog="true";

    this.router.navigate(['./app-login']);
  }

  reg(){
  //   this.isAccountRecover = "false";
    this.istruereg="true";
    this.istruelog="false";

    this.router.navigate(['./app-registration']);
  }
    getstyle_log(){
if(this.istruelog =="true"){
    return "whitesmoke"}
  }

getborder_color_log(){
if(this.istruelog =="true"){
    return "whitesmoke"}
  }
getborder_color_reg(){
if(this.istruereg =="true"){
    return "whitesmoke"}
  }
  getstyle_reg()
{
  if(this.istruereg =="true"){
    return "whitesmoke"}
}
  
  //  Forgot_password(){
  //   this.ss.flag_terminate=false;
  //   this.ss.flag_change_password=false;
  //   this.ss.flag_forgot_password=true;
  //   return this.modal.open(AccountModalComponent,  overlayConfigFactory({}, BSModalContext));
  // }

  // forgotPassword(){
  //   this.isAccountRecover = "true";

  //   this.router.navigate(['./account-recover']);
  // }


  // getstyle_log(){
  //   if(this.istruelog =="true"){
  //   return "#3A3D3D"}
  // }

  // getstyle_reg(){
  //   if(this.istruereg =="true"){
  //   return "#3A3D3D"}
  // }

  // getfontsize_log(){
  //   if(this.istruelog =="true"){
  //     return "30px"}
  // }

  // getfontsize_reg(){
  //   if(this.istruereg =="true"){
  //     return "30px"}
  // }


}
