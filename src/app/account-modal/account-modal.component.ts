import { Component, EventEmitter,OnInit,Output } from '@angular/core';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';
import { BSModalContext } from 'angular2-modal/plugins/bootstrap';
// import{MockAssetService} from '../services/mock_asset.service';
import{SharedService} from '../services/shared.service';
import{AccountService} from '../services/account.service';
import{CampaignModel} from '../model/campaign.model';
import { ToastrService } from 'toastr-ng2';
declare var $;
export class CustomModalContext extends BSModalContext {
  
} 
@Component({
  selector: 'app-account-modal',
  templateUrl: './account-modal.component.html',
  styleUrls: ['./account-modal.component.css']
})
export class AccountModalComponent implements OnInit , CloseGuard, ModalComponent<CustomModalContext> {
    context: CustomModalContext;
      name:any;
      img:string;
      description:string;
      CurrentPassword: string;
      NewPassword: string;
      changePasswordObj: {};
      recoveryEmailId:string;
      recovery:{};
      
    //  flag_asset=this.sharedSvc.flag_asset;
    // flag_chng_pwd=this.sharedSvc.flag_change_password;
    // flag_forgt_pwd=this.sharedSvc.flag_forgot_password;
    // flag_terminate=this.sharedSvc.flag_terminate;
  
      mock:any[]=[];

      constructor( public toaster : ToastrService, public accountSvc : AccountService,  public sharedSvc : SharedService , public dialog: DialogRef<CustomModalContext>) {
        this.CurrentPassword = "";
        this.NewPassword ="";
        this.changePasswordObj = {CurrentPassword: this.CurrentPassword, NewPassword : this.NewPassword};
        this.recovery = {recoveryEmailId: this.recoveryEmailId};
       }


        ngOnInit() {

          
         }

        onClose() {
          this.dialog.close();
              $('body').removeClass('modal-open');
        }
        terminate(){
          this.onClose();
        }
        change_pwd(){
          
          this.accountSvc.changePassword(this.changePasswordObj).then(res =>{
            console.log(res);
            if(res.status){
              this.toaster.success('Password saved successfully');
              
            }
            else{
              this.toaster.error('Password not saved');
            }
          });


          this.onClose();
        }
        recoveryPassword(recoveryEmailId){
          console.log(recoveryEmailId);
          this.accountSvc.recoverPasswordByEmailId(recoveryEmailId).then(res=>{
            if(res.status){
              this.toaster.success('Recovery link is sent to your email.');
            }
            else{
              this.toaster.error("Error sending recovery link to your email.");
              this.toaster.error("status code: "+res.statusCode);
            }
            console.log(res);
          })
          this.onClose();
        }


}
