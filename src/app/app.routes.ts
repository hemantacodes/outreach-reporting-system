import {ModuleWithProviders} from '@angular/core';
import { Routes, RouterModule} from '@angular/router';


import { LandingPageComponent } from './landing-page/landing-page.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {AccountrecoverComponent} from './accountrecover/accountrecover.component';





export const router : Routes=[
    {path : '' , component : LandingPageComponent ,
        children : [
            {path : '', redirectTo : 'app-login', pathMatch:'full'},
            {path : 'app-login' , component : LoginComponent},
            {path : 'app-registration' , component : RegistrationComponent}
            
        ]
    },
   {path : 'member' , loadChildren : './member/member.module#MemberModule'},
   {path : 'guest' , loadChildren : './guest/guest.module#GuestModule'},
   {path : 'account-recover/:accountId' , component : AccountrecoverComponent}

]
export const routes : ModuleWithProviders = RouterModule.forRoot(router);