import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core';
import { HttpModule , JsonpModule } from '@angular/http';
import {routes} from './app.routes';
import { FormsModule } from '@angular/forms';
import {ModalModule} from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { DialogRef, ModalComponent, CloseGuard } from 'angular2-modal';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AccountModalComponent } from './account-modal/account-modal.component';

import { BaseService } from './services/base.service';
import {SharedService} from './services/shared.service';
import {AccountService} from './services/account.service';
import {UtilService} from './services/util.service';
import {StateData} from './providers/StateData';

import { CKEditorModule } from 'ng2-ckeditor';
//import { AssetviewComponent } from './member/assetview/assetview.component';
import { PrintAssetModalComponent } from './member/print-asset-modal/print-asset-modal.component';
//import { PrintAssetTemplateComponent } from './member/print-asset-template/print-asset-template.component';
import {AuthenticationService} from './services/authentication.service';
import { ToastrModule } from 'toastr-ng2';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    LandingPageComponent,
    AccountModalComponent,
    PrintAssetModalComponent   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routes,
    HttpModule,
    JsonpModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    FormsModule,
    CKEditorModule,
    ToastrModule.forRoot()
  ],

  providers: [BaseService, SharedService, AccountService, AuthenticationService,StateData, UtilService],
  entryComponents : [AccountModalComponent,PrintAssetModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }

