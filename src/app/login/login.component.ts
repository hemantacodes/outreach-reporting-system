import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { CreateCampaignModalComponent } from '../create-campaign-modal/create-campaign-modal.component';
import { Overlay, overlayConfigFactory } from 'angular2-modal';
import { Modal, BSModalContext } from 'angular2-modal/plugins/bootstrap';
import { SharedService } from '../services/shared.service';
import {NgForm,NgControl,NG_VALIDATORS,Validators,AbstractControl,ValidatorFn} from '@angular/forms';

import { UtilService } from '../services/util.service';
import { User} from '../model/user.model';
import {AuthenticationService} from '../services/authentication.service';
import {StateData} from '../providers/StateData';
import { ToastrService } from 'toastr-ng2';

declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[User]
})
export class LoginComponent implements OnInit {
  submitted = false;
  user: User = new User;
  public failedLogin :{
    //isFailedLogin: boolean,
    statusMessage:string
  }
  constructor(public utilService: UtilService, public toastrSvc : ToastrService, public stateData : StateData, public ss : SharedService,public router : Router,public modal : Modal , public authService: AuthenticationService) { }
 
  ngOnInit() {
      this.failedLogin ={
        //isFailedLogin: false,
        statusMessage:""
      }
  }

  doLogin(){
    if(this.user.EmailId && this.user.Password){
      this.authService.doLogIn(this.user).then(res =>{
        if(res.status){
          this.router.navigate(['./member/home']);
          console.log(res);
          this.stateData.loggedUser = res.data;
          
        }
        else{
            //this.failedLogin.isFailedLogin = true;
            this.failedLogin.statusMessage = "User authentication failed."
            //this.failedLogin.statusMessage = res.statusMessage
            //this.toastrSvc.error("User authentication failed");
            console.log(res);
        }
      });
      this.submitted=true;
    }
    else{
      this.failedLogin.statusMessage = "Please enter username and password";
    }
  }

  Forgot_password(){
    //this.ss.flag_forgot_password=true;
    this.utilService.setConfirmModalArg("flag_forgot_password");
    return this.modal.open(CreateCampaignModalComponent,  overlayConfigFactory({}, BSModalContext));
  }

  clearFailMessage(){
    this.failedLogin.statusMessage="";
  }
}
