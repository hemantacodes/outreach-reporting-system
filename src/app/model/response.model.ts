export class ResponseModel {
    data: any
    status: boolean
    statusCode: number
    statusMessage: string
}