// export interface Assets{
//     a_name:string;
//     descrp:string;
//     image:string;
//     qr:string;
//     no_qr:string;
//     hasQRCode : boolean;
//     qr_code_value:string;
// }
import {Asset} from "../model/asset.model";
import {Subscription} from "../model/subscription.model";

export class CampaignModel{
    id:string;
    campaignName: string;
    //Account: any;
    assetList: Array<Asset>;
    subscription: Subscription;
    isValidSubscription : boolean;




////////////old/////////////
    // c_name:string;
    // id:number;
    // assets:Array <Asset>;
    
    // hasSubscription : boolean;
    // subscriptionExpiryDate : any;
    //  assets:any = []


}