import { User} from '../model/user.model';
import {Constants} from '../model/constants.model';

export class LoggedUser{
   role: string;
   lastLogin : string;
   userName : string
}