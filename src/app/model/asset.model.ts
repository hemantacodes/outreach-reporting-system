export class Asset{
    // id: string;
    // createdDate:string;
    // lastModifiedDate: string;
    // name:string;
    // metaData:string;
    // campaignId: string;
    

    // a_name:string;
    // descrp:string;
    // image:string;
    // qr:string;
    // no_qr:string;
    // hasQRCode : boolean;
    // qr_code_value:string;

    ////////////////
    id :string;
    name : string;
    metaData : string;
    qrCode : string;
    qrCodeImageURL : string;
    feedbackCount : number;
    viewCount : number;
}