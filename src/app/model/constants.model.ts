export class Constants{
    static LOGIN_RESPONSE : string = "loginResponse"; 
    static CURRENT_STATE: string = "currentState";
    static NULL_GUID: string = "00000000-0000-0000-0000-000000000000";
}
