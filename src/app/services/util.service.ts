import { Injectable } from '@angular/core';

import { Http, Response, Headers, RequestOptions, } from '@angular/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

import {Constants} from '../model/constants.model';

@Injectable()
export class UtilService {
    
    // public subscrptn = {
    //     expiryDate:"",
    //     assetCount:""
    // }

    flag_create:boolean;
    flag_delete_asset:boolean;
    flag_delete_single:boolean;
    flag_single_feedback_delete:boolean;
    flag_all_feedback_delete:boolean;
  
    flag_forgot_password:boolean;
    flag_change_password:boolean;
    flag_terminate:boolean;

    constructor(){
        
    }

    public setConfirmModalArg(key){
        switch (key) {
            case "flag_create": {
                                            this.flag_create = true;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = false;
                                            this.flag_terminate = false;
                                        }
                
                break;
            case "flag_delete_asset":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = true;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = false;
                                            this.flag_terminate = false;
                                        }
                break;
            case "flag_delete_single":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = true;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = false;
                                            this.flag_terminate = false;
                                        }
                break;
            case "flag_single_feedback_delete":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = true;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = false;
                                            this.flag_terminate = false;
                                        }
                break;
            case "flag_all_feedback_delete":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = true;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = false;
                                            this.flag_terminate = false;
                                        }
                break;
            case "flag_forgot_password":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = true;
                                            this.flag_change_password = false;
                                            this.flag_terminate = false;
                                        }
                break;
            case "flag_change_password":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = true;
                                            this.flag_terminate = false;
                                        }
                break;
            case "flag_terminate":   {
                                            this.flag_create = false;
                                            this.flag_delete_asset = false;
                                            this.flag_delete_single = false;
                                            this.flag_single_feedback_delete = false;
                                            this.flag_all_feedback_delete = false;
                                            this.flag_forgot_password = false;
                                            this.flag_change_password = false;
                                            this.flag_terminate = true;
                                        }
                break;
            
            default:
                break;
        }
    }

}