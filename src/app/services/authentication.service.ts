import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs';
import {Jsonp} from '@angular/http';
import {ResponseModel} from '../model/response.model';
import { environment } from '../../environments/environment';
import { BaseService } from './base.service';
//import { AuthResponseModel } from '../models/authResponse.model';
import {Constants} from '../model/constants.model';
import { Router } from '@angular/router';
import {User} from "../model/user.model";
import {StateData} from '../providers/StateData';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class AuthenticationService extends BaseService {
   private authUrl : string = environment.BASE_URL + "authenticate";

    constructor(public stateData: StateData , http: Http , public router : Router) {
        super(stateData,http)
    }

   /*doLogIn(obj) : Promise<boolean> {
      //this.objResponse = false;
      return this.invokeService(this.authUrl,obj,1)
        .then(data => {
            localStorage.setItem(Constants.LOGIN_RESPONSE,JSON.stringify(data.data));
            return data.status;
        });
   }*/

    doLogIn(obj) : Promise<ResponseModel> {
      //this.objResponse = false;
      return this.invokeService(this.authUrl,obj,1)
        .then(data => {
          if(data.status){
            localStorage.setItem(Constants.LOGIN_RESPONSE,JSON.stringify(data.data));
            this.stateData.loggedUser = data.data;
          }
          return data;
        });
   }

}
