import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';

import {ResponseModel} from '../model/response.model';
import {BaseService} from '../services/base.service';
import {StateData} from '../providers/StateData';

@Injectable()
export class AccountService extends BaseService{

  constructor(public stateData: StateData, http: Http) {
    super(stateData, http)
   }

  //private baseUrl:string =  environment.BASE_URL;
   private accountURL: string = this.baseUrl + "account";

  createAccount(acc) : Promise<ResponseModel>{
       return this.invokeService(this.accountURL,acc,this.post);
  }

  acitvateAccount(obj) : Promise<ResponseModel>{
       return this.invokeService(this.accountURL+'someurl/activateAcc',obj,1);
  }

  getAccountInfo() : Promise<ResponseModel>{
       return this.invokeService(this.accountURL+'someurl/getAccInfo','',2);
  }

  deleteAccount(obj) : Promise<ResponseModel>{
       return this.invokeService(this.accountURL+'someurl/deleteAccount',obj,1);
  }

  recoverPasswordByEmailId(emailId) : Promise<ResponseModel>{
       return this.invokeService(this.accountURL+'/'+emailId+'/Forgot',emailId,this.post);
  }
  
  changePassword(changePasswordObj) : Promise<ResponseModel>{
       return this.invokeService(this.accountURL+'/ResetPassword',changePasswordObj,this.post);
  }
  changePasswordByAccountId(changePasswordObj) : Promise<ResponseModel>{
    return this.invokeService(this.accountURL+'/ForgotPassword',changePasswordObj,this.post);
}
}
