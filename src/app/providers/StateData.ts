import { Injectable } from '@angular/core';

import { CampaignModel } from '../model/Campaign.model';
import { Asset } from '../model/Asset.model';
import { LoggedUser} from '../model/loggeduser.model';
import { Feedback} from '../model/Feedback.model';

@Injectable()
export class StateData {
    public loggedUser : LoggedUser;
    public selectedCampaign: CampaignModel;
    public selectedAsset: Asset;
    public selectedFeedback: Feedback;
    public feedbackList : Feedback[];

    public campaignList : CampaignModel[];
    //public campaignName: string;
    //public otherData: any;
    public previousPage: string;
    //public userConfirmation : boolean;

    public constructor() { }

    getLastLoginDate

}
