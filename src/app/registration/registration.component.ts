import { Component, OnInit } from '@angular/core';
import {NgForm,NgControl,NG_VALIDATORS,AbstractControl,ValidatorFn,FormGroup, FormControl, Validators} from '@angular/forms';
// import { FORM_DIRECTIVES, ControlGroup, Control, Validators, FormBuilder, Validator, } from '@angular/common';
import 'rxjs/Rx';
import { Account } from '../model/account.model'; 
import {User} from "../model/user.model";
import{AccountService} from '../services/account.service';
import { ToastrService } from 'toastr-ng2';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers : [Account]
})
export class RegistrationComponent implements OnInit {

  account : Account = new Account();
  public failedRegistration :{
    isFailedRegistration: boolean,
    statusMessage:string
  }

  constructor(private accServ: AccountService, public toaster : ToastrService) {
    this.account.user = new User();
    //this.account.user.EmailId = "testemail";
  }
  // username:string;
  // email:string;
  // password:string;
  // //phone_number:string;
  // conf_email:string
  // myform: FormGroup;
   validationMessage:string;
   

  ngOnInit() {
    
    this.validationMessage = "";

    this.failedRegistration ={
        isFailedRegistration: false,
        statusMessage:""
      }
  }



  // checkConfirmationEmail(){
    
  //     if(this.account.user.EmailId != this.account.ConfirmEmailId){
  //       return true;
  //     }
  //     else{
  //       return false;
  //     }
    
  // }

  doRegistration(){
    if(this.account.user.EmailId == this.account.ConfirmEmailId){
        //call register service
        console.log(this.account);
        this.accServ.createAccount(this.account).then(res=>{
          if(res.status){
            this.toaster.success('Account Registered');
            console.log("success");
            
          }
          else{
            this.toaster.error('Error!!');
            console.log(" not success");
            this.failedRegistration.statusMessage="Error on registering this account."
            this.failedRegistration.isFailedRegistration = true;
          }
        },
            error => {
          this.toaster.error('Error!!');
          console.log(" not success");
          this.failedRegistration.statusMessage="Error on registering this account."
          this.failedRegistration.isFailedRegistration = true;
        })
    }
    else{
      this.failedRegistration.statusMessage="Email addresses are not same."
      this.failedRegistration.isFailedRegistration = true;
      //this.failedRegistration.statusMessage
    }
  }

  clearFailMessage(){
    this.failedRegistration.statusMessage = "";
    this.failedRegistration.isFailedRegistration = false;
  }
}